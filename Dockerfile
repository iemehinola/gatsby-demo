FROM node:latest

RUN mkdir -p /app

WORKDIR /app

COPY .  /app

RUN npm install -g gatsby-cli

RUN npm install

RUN gatsby build

EXPOSE 5000

CMD ["gatsby", "serve", "-p", "5000", "-H", "0.0.0.0"]